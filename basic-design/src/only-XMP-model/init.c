#include <stdio.h>
#include "Matrix.xmptype.h"

#pragma xmp nodes _XMP_default_nodes(2,2)
#pragma xmp template t(0:3,0:3)
#pragma xmp distribute t(block,block) onto _XMP_default_nodes

XMP_Matrix A[4][4];
#pragma xmp align A[i][j] with t(j,i)

int main(int argc, char ** argv){
 
  // Computation code
  int i,j,nx,ny,n;
  int rank;
  int size;
  nx=2;
  ny=2;
  n=nx*ny;
  
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

#pragma xmp loop (i,j) on t(j,i)
  for (i=0;i<n;i++){
    for(j=0;j<n;j++){
      A[i][j] = i*n + j;
      fprintf(stderr,"\n(%d, %d, %d) %.1f",rank,i,j, A[i][j]);
    }
  }
  
  
  // Local alias
  double Aloc[4];
  int Nprocs_i = 2;
  int Nprocs_j = 2;
  int Nprocs;
  int k1D;
  int ki, kj;
  Nprocs = Nprocs_i*Nprocs_j;

  int istart[Nprocs];
  int jstart[Nprocs];
  int my_istart, my_jstart;

  // Get global coordinate according to rank
  for (ki=0;ki<Nprocs_i;ki++){
    for (kj=0;kj<Nprocs_j;kj++){
      k1D = ki*Nprocs_i + kj;
      istart[k1D]=ki*nx;
      jstart[k1D]=kj*ny;
    }
  }
  my_istart=istart[rank];
  my_jstart=jstart[rank];

  // Get logal array for communication
  Aloc[0:4]=A[my_istart][my_jstart:my_jstart+ny-1];
  printf("\nrank=%d, (istart,jstart)=(%d,%d) , Aloc[0:3] = [%.1f %.1f %.1f %.1f] \n", rank,my_istart, my_jstart,Aloc[0],Aloc[1],Aloc[2],Aloc[3]);
  
}
  
  
