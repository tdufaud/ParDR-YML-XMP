/******************************************************************************
* Copyright Thomas Dufaud, Miwako Tsuji 2017
* 
* Thomas Dufaud 
*       thomas.dufaud@uvsq.fr
*       University of Versailles Saint-Quentin-en-Yvelines (UVSQ)
*       45 avenue des Etats Unis, 78035 Versailles Cedex FRANCE 
* 
* Miwako Tsuji 
*       miwako.tsuji@riken.jp
*       RIKEN R-CCS Center for Computational Science
*       7-1-26 Minatojima-minami-machi, Chuo-ku, Kobe, Hyogo 650-0047, Japan
*
* This software is a computer program whose purpose is to study data
* management for Multi SPMD programming
*
* This software is governed by the [CeCILL] license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the [CeCILL]
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<mpi.h>
#include"misc.h"

#define N 4 // This is the number of servers and is equall to the size
            // of a a worker (a client of data-server)  
            // This is temporary specified here 



int main(int argc, char **argv)
{
  int      i; 
  int      myrank, nprocs;
  int      ndr = N;
  char     cmd[256], **arg;
  MPI_Comm dr_comm[N];
  MPI_Comm wk_comm;
  
  MPI_Init(&argc, &argv);

  arg = ch_calloc(2, 64);
  
  // invoke servers
  sprintf(cmd,"./server");
  for(i=0; i<N; i++){
    sprintf(arg[0],"%d\n",i);
    MPI_Comm_spawn(cmd, arg, 1, MPI_INFO_NULL, 0, MPI_COMM_SELF, &(dr_comm[i]), MPI_ERRCODES_IGNORE);
  }

  // invoke workers
  memset(cmd, 0, sizeof(cmd));
  sprintf(cmd, "./worker");
  MPI_Comm_spawn(cmd, arg, N, MPI_INFO_NULL, 0, MPI_COMM_SELF, &(wk_comm), MPI_ERRCODES_IGNORE);

  free(*arg); free(arg);
  
  MPI_Finalize();

  return 0;

} /* main */

