/******************************************************************************
* Copyright Thomas Dufaud, Miwako Tsuji 2017
* 
* Thomas Dufaud 
*       thomas.dufaud@uvsq.fr
*       University of Versailles Saint-Quentin-en-Yvelines (UVSQ)
*       45 avenue des Etats Unis, 78035 Versailles Cedex FRANCE 
* 
* Miwako Tsuji 
*       miwako.tsuji@riken.jp
*       RIKEN R-CCS Center for Computational Science
*       7-1-26 Minatojima-minami-machi, Chuo-ku, Kobe, Hyogo 650-0047, Japan
*
* This software is a computer program whose purpose is to study data
* management for Multi SPMD programming
*
* This software is governed by the [CeCILL] license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the [CeCILL]
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<mpi.h>
#include<unistd.h>
#define NBW 4

int main(int argc, char **argv)
{
  int      myrank, nprocs;
  int      myid;
  char     msg[256];
  char     port[MPI_MAX_PORT_NAME];
  char     service_name[MPI_MAX_PORT_NAME];
  MPI_Comm   myclient;
  MPI_Status stat;
  MPI_Info   info;

  char *data_loc; // data handle by server
  int count; // size of data_loc
 
  int request=0; // -1 := stop listening, 0 := listen for client import, 1 := listen for client export
  int request_status;

  MPI_Init(&argc, &argv);

  if(argc != 2){
    printf("usage: mpirun -n <> ./server <sererid>\n");
    MPI_Finalize();
  }
  myid = atoi(argv[1]);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (nprocs>1){
    myid = myrank;
  }

  //printf("server-%04d: myrank=%4d nprocs=%4d\n",myid,myrank,nprocs);
  int dataId = 0;
  int dataCoord = 0;
  memset(service_name, 0, sizeof(char)*MPI_MAX_PORT_NAME);
  sprintf(service_name, "serv%d%d%d", dataId, dataCoord, myid);

  // In case of global scope requred for MPI_Lookup_name 
  //MPI_Info_create(&info); 
  //MPI_Info_set(info, "ompi_global_scope", "true");
  MPI_Open_port(MPI_INFO_NULL, port);
  MPI_Publish_name(service_name, MPI_INFO_NULL, port);
  printf("server-%04d_%d: opend port: %s %s\n",myid, myrank, service_name, port);
 
  // Allocation considering info sent by the worker at initialization
  MPI_Comm_accept(port, MPI_INFO_NULL, 0, MPI_COMM_SELF, &myclient);
  MPI_Recv(&count, 1, MPI_INT, 0, 0, myclient, &stat);
  data_loc = (char *)malloc(sizeof(char)*count);
  MPI_Comm_disconnect(&myclient);

  int iter=0;
  int countW = 0;

  while ((request != -1)||(countW<NBW)) {
    iter++;
 
    MPI_Comm_accept(port, MPI_INFO_NULL, 0, MPI_COMM_SELF, &myclient);
   
    // Receive listening request
    MPI_Recv(&request, 1, MPI_INT, 0, 0, myclient, &stat);
    if (request == -1) countW++;
    
    // Receive a bloc matrix (client calls for export)
    if (request == 1) {
      
      MPI_Recv(data_loc, count, MPI_CHAR, 0, 0, myclient, &stat);
            
      // Notify
      request_status = 1;
      MPI_Send(&request_status, 1, MPI_INT, 0, 0, myclient);
    }

    // Send a bloc matrix (client calls for import)
    if (request == 0) {
      MPI_Send(data_loc, count, MPI_CHAR, 0, 0, myclient);
    }

    //sleep(1);

    MPI_Comm_disconnect(&myclient);

  }

  if ((request == -1)&&(countW==NBW)) {
    fprintf(stderr,"server-%04d: Stop import/export \n", myid);
  }

  MPI_Unpublish_name(service_name, MPI_INFO_NULL, port);
  
  MPI_Finalize();

  return 0;

} /* main */
  
