/******************************************************************************
* Copyright Thomas Dufaud, Miwako Tsuji 2017
* 
* Thomas Dufaud 
*       thomas.dufaud@uvsq.fr
*       University of Versailles Saint-Quentin-en-Yvelines (UVSQ)
*       45 avenue des Etats Unis, 78035 Versailles Cedex FRANCE 
* 
* Miwako Tsuji 
*       miwako.tsuji@riken.jp
*       RIKEN R-CCS Center for Computational Science
*       7-1-26 Minatojima-minami-machi, Chuo-ku, Kobe, Hyogo 650-0047, Japan
*
* This software is a computer program whose purpose is to study data
* management for Multi SPMD programming
*
* This software is governed by the [CeCILL] license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the [CeCILL]
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
******************************************************************************/
#define PROCI 4
#define Nx 1000
#define N Nx*PROCI
#define NbEx 10

#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include <unistd.h>

#include "Matrix.xmptype.h"
#include "client_spec.h"

#pragma xmp nodes _XMP_default_nodes(PROCI,PROCI)
#pragma xmp template t(0:N-1,0:N-1)
#pragma xmp distribute t(block,block) onto _XMP_default_nodes

XMP_Matrix A[N][N];
#pragma xmp align A[i][j] with t(j,i)

int main(int argc, char ** argv){
 
  // Decl for Computation code
  int i,j,nx,ny,n;
  int rank;
  int size;

  // Decl for Local alias
  int Nprocs_i = PROCI;
  int Nprocs_j = PROCI;
  int Nprocs;
  int k1D;
  int ki, kj;
  Nprocs = Nprocs_i*Nprocs_j;

  int istart[Nprocs];
  int jstart[Nprocs];
  int my_istart, my_jstart;

  // Decl for import/export code
  char     port[MPI_MAX_PORT_NAME];
  char     service_name[MPI_MAX_PORT_NAME];
  MPI_Comm myserver;
  MPI_Status stat;

  int total_count; // size of data to sent n*sizeof(datatype)
  int request; // -1 := stop listening, 0 := listen for client import, 1 := listen for client export

  // Decl for global data and server name
  int dataId = 0;
  int dataCoord = 0;

  // Decl for client id
  int clientId;
  if (argc) clientId=atoi(argv[1]);
  
  // elapsed time control
  int kt; // iter for number of exchange 
  double start_time, end_time, tmp_time, elapsed_time;
  double *result_time;
  double w_time;

  nx=Nx;
  ny=nx;
  n=nx*Nprocs_i;
  
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

#pragma xmp loop (i,j) on t(j,i)
  for (i=0;i<n;i++){
    for(j=0;j<n;j++){
      A[i][j] = i*n + j;
      //fprintf(stderr,"\n(%d, %d, %d) %.1f",rank,i,j, A[i][j]);
    }
  }
  
  // Get global coordinate according to rank
  Nprocs = Nprocs_i*Nprocs_j;
  for (ki=0;ki<Nprocs_i;ki++){
    for (kj=0;kj<Nprocs_j;kj++){
      k1D = ki*Nprocs_i + kj;
      istart[k1D]=ki*nx;
      jstart[k1D]=kj*ny;
    }
  }
  my_istart=istart[rank];
  my_jstart=jstart[rank];
  
  // Initialize size of data on server side 
  // This should not be handle by a worker, when integrated in YML
  connectDR( dataId, dataCoord, rank, service_name, port, &myserver);
  total_count=nx*ny*sizeof(double);
  MPI_Send(&total_count, 1, MPI_INT, 0, 0, myserver);
  disconnectDR( &myserver);

  elapsed_time=0.0;
  tmp_time=0.0;

  for (kt=0;kt<NbEx;kt++){

    // Export A
    start_time = MPI_Wtime();
    connectDR( dataId, dataCoord, rank, service_name, port, &myserver);
    export(A[my_istart], nx*ny, MPI_DOUBLE, 0, myserver);
    disconnectDR( &myserver);
    end_time = MPI_Wtime();
  
    tmp_time = end_time - start_time;


#pragma xmp loop (i,j) on t(j,i)
    for (i=0;i<n;i++){
      for(j=0;j<n;j++){
	A[i][j] = 0.;
	//fprintf(stderr,"\n(%d, %d, %d) %.1f",rank,i,j, A[i][j]);
      }
    }

    // Import A
    start_time = MPI_Wtime();
    connectDR( dataId, dataCoord, rank, service_name, port, &myserver);
    import(A[my_istart], nx*ny, MPI_DOUBLE, 0, myserver);
    disconnectDR( &myserver);
    end_time = MPI_Wtime();

    elapsed_time = elapsed_time + tmp_time + (end_time - start_time);
  }

  result_time = (double *)malloc(sizeof(double)*size);
  MPI_Allgather(&elapsed_time, 1, MPI_DOUBLE, result_time, 1, MPI_DOUBLE, MPI_COMM_WORLD);
  
  w_time = 0.0;
  if (rank==0){
    for (i=0;i<size;i++){
      if (w_time<result_time[i]){
	w_time = result_time[i]; 
      }
    }
    printf("\n C-%d Wall time= %lf s \n", clientId, w_time);
  }
  printf("\n C-%d rank %d final values A[0:3]=[%.1f %.1f %.1f %.1f]\n", clientId, rank, A[my_istart][my_jstart],A[my_istart][my_jstart+1],A[my_istart][my_jstart+2],A[my_istart][my_jstart+3]);
  // Stop import/export definitly
  connectDR( dataId, dataCoord, rank, service_name, port, &myserver);
  request = -1;
  MPI_Send(&request, 1, MPI_INT, 0, 0, myserver);
  disconnectDR(&myserver);

  return 0;  
}
