/******************************************************************************
* Copyright Thomas Dufaud, Miwako Tsuji 2017
* 
* Thomas Dufaud 
*       thomas.dufaud@uvsq.fr
*       University of Versailles Saint-Quentin-en-Yvelines (UVSQ)
*       45 avenue des Etats Unis, 78035 Versailles Cedex FRANCE 
* 
* Miwako Tsuji 
*       miwako.tsuji@riken.jp
*       RIKEN R-CCS Center for Computational Science
*       7-1-26 Minatojima-minami-machi, Chuo-ku, Kobe, Hyogo 650-0047, Japan
*
* This software is a computer program whose purpose is to study data
* management for Multi SPMD programming
*
* This software is governed by the [CeCILL] license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the [CeCILL]
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
******************************************************************************/
/*
  Specification of a client 
  - connect and disconnect DR
  - import from / export to  DR
  - remap: prevent pdr for remapping of data
*/  
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "client_spec.h"

int connectDR(int dataId, int dataCoord, int client_rank, void *service_name, void *port, MPI_Comm *server_comm){
  int ierr;
  memset (service_name, 0, sizeof(char)*MPI_MAX_PORT_NAME);
  sprintf(service_name, "serv%d%d%d", dataId, dataCoord, client_rank);
  //printf("client-rank=%4d: connecting to server%d %s...\n",client_rank,client_rank,(char *) service_name); fflush(stdout);
  ierr = MPI_Lookup_name(service_name, MPI_INFO_NULL, port);
  if (ierr!=MPI_SUCCESS) fprintf(stderr,"After Lookup Name IERR = %d \n", ierr);
  //printf("client-rank=%4d: connecting to server%d %s... Lookup_name done port=%s\n",client_rank,client_rank,(char *) service_name,(char *) port); fflush(stdout);
  ierr = MPI_Comm_connect(port, MPI_INFO_NULL, 0, MPI_COMM_SELF, server_comm);
  if (ierr!=MPI_SUCCESS) fprintf(stderr,"After connectionn IERR = %d \n", ierr);
  printf("client-rank=%4d: connecting to server%d %s...done \n",client_rank,client_rank,(char *) service_name); fflush(stdout);
  return 0;
}

int disconnectDR(MPI_Comm *server_comm){
  MPI_Comm_disconnect(server_comm);
  return 0;
}

int export(void *buf, int count, MPI_Datatype datatype, int tag, MPI_Comm server_comm){
  int request_status = 0; // 0 := export is not done, 1 := export is done  
  int request = 1; // 1 := server listen for client export
  MPI_Status stat;

  while (request_status==0) {
    // Ask for export
    MPI_Send(&request, 1, MPI_INT, 0, tag, server_comm);

    // Send data
    MPI_Send(buf, count, datatype, 0, tag, server_comm);

    // Receive notification
    MPI_Recv(&request_status, 1, MPI_INT, 0, tag, server_comm, &stat);
  }
  return 0;
}

int import(void *buf, int count, MPI_Datatype datatype, int tag, MPI_Comm server_comm){
  int request = 0; // 0 := server listen for client import
  MPI_Status stat;

  // Ask for import
  MPI_Send(&request, 1, MPI_INT, 0, tag, server_comm);
  
  // Receive message
  MPI_Recv(buf, count, datatype, 0, tag, server_comm, &stat);

  return 0;
}

int remap(int dataId, int dataCoord, void *originMapping, void *newMapping, int tag, MPI_Comm server_comm){
  int request_status = 0; // 0 := remap is not done, 1 := remap is done  
  int request = 2; // 2 := server listen for client remapping
  MPI_Status stat;

  while (request_status==0) {
    // Ask for export
    MPI_Send(&request, 1, MPI_INT, 0, tag, server_comm);

    MPI_Send(originMapping, 12, MPI_CHAR, 0, tag, server_comm);
    MPI_Send(newMapping, 12, MPI_CHAR, 0, tag, server_comm);

    // Remapping is done in PDR (see pdr code)

    // Receive notification
    MPI_Recv(&request_status, 1, MPI_INT, 0, tag, server_comm, &stat);
  }
  return 0;
}

int remapIE(int dataId, int dataCoord, void *originMapping, void *newMapping, void *ieType, void *buf, int count, MPI_Datatype datatype, int tag, MPI_Comm server_comm){

  int request_status = 0; // 0 := remap is not done, 1 := remap is done  
  int request = 3; // 3 := server listen for client remapping + import or export
  MPI_Status stat;

  while (request_status==0) {
    // Ask for export
    MPI_Send(&request, 1, MPI_INT, 0, tag, server_comm);

    MPI_Send(originMapping, 12, MPI_CHAR, 0, tag, server_comm);
    MPI_Send(newMapping, 12, MPI_CHAR, 0, tag, server_comm);
    MPI_Send(ieType, 12, MPI_CHAR, 0, tag, server_comm);

    if ( !(strcmp(ieType,"export")) ){
      // Send data
      MPI_Send(buf, count, datatype, 0, tag, server_comm);

      // Receive notification
      MPI_Recv(&request_status, 1, MPI_INT, 0, tag, server_comm, &stat);
    }

    if ( !(strcmp(ieType,"import")) ){
      // Receive message
      MPI_Recv(buf, count, datatype, 0, tag, server_comm, &stat);
      request_status=1;
    }
    // Remapping is done in PDR (see pdr code)

  }

  return 0;
}
