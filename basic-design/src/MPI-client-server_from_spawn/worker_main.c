/******************************************************************************
* Copyright Thomas Dufaud, Miwako Tsuji 2017
* 
* Thomas Dufaud 
*       thomas.dufaud@uvsq.fr
*       University of Versailles Saint-Quentin-en-Yvelines (UVSQ)
*       45 avenue des Etats Unis, 78035 Versailles Cedex FRANCE 
* 
* Miwako Tsuji 
*       miwako.tsuji@riken.jp
*       RIKEN R-CCS Center for Computational Science
*       7-1-26 Minatojima-minami-machi, Chuo-ku, Kobe, Hyogo 650-0047, Japan
*
* This software is a computer program whose purpose is to study data
* management for Multi SPMD programming
*
* This software is governed by the [CeCILL] license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the [CeCILL]
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability. 
*
* In this respect, the user's attention is drawn to the risks associated
* with loading,  using,  modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean  that it is complicated to manipulate,  and  that  also
* therefore means  that it is reserved for developers  and  experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or 
* data to be ensured and,  more generally, to use and operate it in the 
* same conditions as regards security. 
* 
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms.
******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<mpi.h>
#include<unistd.h>
#include "client_spec.h"

int main(int argc, char **argv)
{
  int      i, myrank, nprocs;
  char     msg[256];
  char     port[MPI_MAX_PORT_NAME];
  char     service_name[MPI_MAX_PORT_NAME];
  MPI_Comm myserver;
  MPI_Status stat;

  int request; // -1 := stop listening, 0 := listen for client import, 1 := listen for client export


  MPI_Init(&argc, &argv);
  
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  printf("client-rank=%4d: nprocs=%4d\n",myrank,nprocs);
 
  // Export test
  connectDR( 0, 0, myrank, service_name, port, &myserver); 
  sprintf(msg,"Hello world form client-%d\n",myrank);
  export(msg, 256, MPI_CHAR, 0, myserver);
  disconnectDR(&myserver);
  
  // Import test
  connectDR( 0, 0, myrank, service_name, port, &myserver); 
  import(msg, 256, MPI_CHAR, 0, myserver);
  printf("client-%04d: message from %04d-th : %s\n",myrank,myrank,msg);
  disconnectDR(&myserver);

  // Stop import/export definitly
  connectDR( 0, 0, myrank, service_name, port, &myserver);
  request = -1;
  MPI_Send(&request, 1, MPI_INT, 0, 0, myserver);
  disconnectDR(&myserver);

  MPI_Finalize();

  return 0;

} /* main */
