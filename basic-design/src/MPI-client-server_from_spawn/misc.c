#include<stdlib.h>
#include<stdio.h>

// allocate (n-1)*m sized matrix and matrix[n-1]=NULL;

char **ch_calloc(int n, int m){
  int    i; 
  char   **a;

  if(n<=1){
    printf("malloc error : n should >= 2\n");
    exit(1);
  }
  
  a = (char **)malloc(sizeof(char*)*(n));
  *a = (char *)calloc(m*(n-1),sizeof(char));
  for(i=0; i<n-1; i++){
    a[i] = &((*a)[i*m]);
  }
  a[n-1]=NULL;
  return a; 
}
			     
