#!/bin/bash
#@ class = clallmds+
#@ job_name = RUN_master-xmpclient
#@ total_tasks = 48
#@ node = 3
#@ as_limit = 1.75gb
#@ node_usage = not_shared
#
#@ wall_clock_limit = 00:05:00
#@ output = $(job_name).$(jobid).log
#@ error = $(job_name).$(jobid).err
#@ job_type = mpich
#@ environment = COPY_ALL
#@ queue
#
module purge
module load gnu/5.4.0 gnu-env/5.4.0 intel intelmpi autotools/Feb2014
export I_MPI_DAPL_PROVIDER=ofa-v2-ib0

echo $LOADL_PROCESSOR_LIST | sed -e s/" "/\\n/g
echo $LOADL_PROCESSOR_LIST | sed -e s/" "/\\n/g > hosts

echo "localhost" > hosts
j=1
while [ $j -lt 48 ]
do
        echo "localhost" >> hosts
        j=$[$j+1]
done


#mpirun -n 1 -machinefile hosts -v -check_mpi -genv I_MPI_DEBUG 5 ./master-xmpclient
time mpirun -n 1 -machinefile hosts ./master-xmpclient
