#!/bin/bash
#@ class = clallmds+
#@ job_name = RUN_master-Cclient
#@ total_tasks = 32
#@ node = 2
#@ as_limit = 1.75gb
#@ node_usage = not_shared
#
#@ wall_clock_limit = 00:03:00
#@ output = $(job_name).$(jobid).log
#@ error = $(job_name).$(jobid).err
#@ job_type = mpich
#@ environment = COPY_ALL
#@ queue
#
module purge
module load gnu/5.4.0 gnu-env/5.4.0 intel intelmpi autotools/Feb2014
export I_MPI_DAPL_PROVIDER=ofa-v2-ib0

echo "localhost" > hosts
j=1
while [ $j -lt 32 ]
do
        echo "localhost" >> hosts
        j=$[$j+1]
done

#cp hosts ~/.omrpc_registry/nodes

mpirun -n 1 --hostfile hosts ./master