#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<mpi.h>
#include"misc.h"

#define N 16 // This is the number of servers and is equall to the size
            // of a a worker (a client of data-server)  
            // This is temporary specified here 

#define NBW 1 // Number of workers spawned

int main(int argc, char **argv)
{
  int      i; 
  int      myrank, nprocs;
  int      ndr = N;
  char     cmd[256], **arg;
  MPI_Comm dr_comm[N];
  MPI_Comm wk_comm;
  
  MPI_Init(&argc, &argv);

  arg = ch_calloc(2, 64);
  
  for (i=0; i<NBW; i++){
    fprintf(stderr,"\n MASTER invoke worker %d \n", i);
    // invoke workers
    memset(cmd, 0, sizeof(cmd));
    sprintf(arg[0],"%d\n",i);
    sprintf(cmd, "./xmp-nfs-worker");
    MPI_Comm_spawn(cmd, arg, N, MPI_INFO_NULL, 0, MPI_COMM_SELF, &(wk_comm), MPI_ERRCODES_IGNORE);
  }
  free(*arg); free(arg);
  
  MPI_Finalize();

  return 0;
} /* main */

