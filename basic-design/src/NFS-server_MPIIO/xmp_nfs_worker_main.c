#define PROCI 4
#define Nx 10
#define N Nx*PROCI
#define NbEx 10
 
#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include<xmp_io.h>
#include "Matrix.xmptype.h"

#pragma xmp nodes _XMP_default_nodes(PROCI,PROCI)
#pragma xmp template t(0:N-1,0:N-1)
#pragma xmp distribute t(block,block) onto _XMP_default_nodes

XMP_Matrix A[N][N];
#pragma xmp align A[i][j] with t(j,i)

int main(int argc, char ** argv){
 
  // Decl for client id
  int clientId;
  if (argc) clientId=atoi(argv[1]);

  // Decl for Computation code
  int i,j,nx,ny,n;
  int rank;
  int size;

  int total_count; // size of data to sent n*sizeof(datatype)
  int request; // -1 := stop listening, 0 := listen for client import, 1 := listen for client export
  char *filename;

  // Decl for Local alias
  int Nprocs_i = PROCI;
  int Nprocs_j = PROCI;
  int Nprocs;
  int k1D;
  int ki, kj;
  Nprocs = Nprocs_i*Nprocs_j;

  int istart[Nprocs];
  int jstart[Nprocs];
  int my_istart, my_jstart;

  // elapsed time control
  int kt; // iter for number of exchange 
  double start_time, end_time, tmp_time, elapsed_time;
  double *result_time;
  double w_time;
  nx=Nx;
  ny=nx;
  n=nx*Nprocs_i;
  
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

#pragma xmp loop (i,j) on t(j,i)
  for (i=0;i<n;i++){
    for(j=0;j<n;j++){
      A[i][j] = i*n + j;
      //fprintf(stderr,"\n(%d, %d, %d) %.1f",rank,i,j, A[i][j]);
    }
  }
  

  // Get global coordinate according to rank
  Nprocs = Nprocs_i*Nprocs_j;
  for (ki=0;ki<Nprocs_i;ki++){
    for (kj=0;kj<Nprocs_j;kj++){
      k1D = ki*Nprocs_i + kj;
      istart[k1D]=ki*nx;
      jstart[k1D]=kj*ny;
    }
  }
  my_istart=istart[rank];
  my_jstart=jstart[rank];

  xmp_desc_t descA;
  descA = xmp_desc_of(A);

  xmp_file_t *fh;
  filename="/gpfsdata/tdufaud/A.dat";

  xmp_range_t *rp;
  rp = xmp_allocate_range(2);
  xmp_set_range(rp,1,0,n,1);
  xmp_set_range(rp,2,0,n,1);
  
  elapsed_time=0.0;
  tmp_time=0.0;

  for (kt=0;kt<NbEx;kt++){
  
    // Export A
    start_time = MPI_Wtime();
    fh = xmp_fopen_all(filename,"w");
    xmp_file_set_view_all(fh,0, descA, rp);
    xmp_fwrite_darray_all(fh,descA,rp);
    xmp_file_clear_view_all(fh,0);
    xmp_fclose_all(fh);
    end_time = MPI_Wtime();
  
    tmp_time = end_time - start_time;

#pragma xmp loop (i,j) on t(j,i)
    for (i=0;i<n;i++){
      for(j=0;j<n;j++){
	A[i][j] = 0.;
	//fprintf(stderr,"\n(%d, %d, %d) %.1f",rank,i,j, A[i][j]);
      }
    }

    // Import A
    start_time = MPI_Wtime();
    fh = xmp_fopen_all(filename,"r");
    xmp_file_set_view_all(fh, 0, descA, rp);
    xmp_fread_darray_all(fh,descA,rp);
    xmp_file_clear_view_all(fh, 0);
    xmp_fclose_all(fh);
    end_time = MPI_Wtime();

    elapsed_time = elapsed_time + tmp_time + (end_time - start_time);  
  }
  
  result_time = (double *)malloc(sizeof(double)*size);
  MPI_Allgather(&elapsed_time, 1, MPI_DOUBLE, result_time, 1, MPI_DOUBLE, MPI_COMM_WORLD);

  w_time = 0.0;
  if (rank==0){
    for (i=0;i<size;i++){
      if (w_time<result_time[i]){
	w_time = result_time[i]; 
      }
    }
    printf("\n C-%d Wall time= %lf s \n", clientId, w_time);
  }

  printf("\n C-%d rank %d final values A[0:3]=[%.1f %.1f %.1f %.1f]\n", clientId, rank, A[my_istart][my_jstart],A[my_istart][my_jstart+1],A[my_istart][my_jstart+2],A[my_istart][my_jstart+3]);

  xmp_free_range(rp);

  return 0;
}
