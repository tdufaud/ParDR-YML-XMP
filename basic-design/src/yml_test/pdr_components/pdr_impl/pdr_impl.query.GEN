<?xml version="1.0"?>
<!-- Copyright Thomas Dufaud, Miwako Tsuji 2017                                    -->
<!--                                                                               -->
<!--  Thomas Dufaud                                                                -->
<!--        thomas.dufaud@uvsq.fr                                                  -->
<!--        University of Versailles Saint-Quentin-en-Yvelines (UVSQ)              -->
<!--        45 avenue des Etats Unis, 78035 Versailles Cedex FRANCE                -->
<!--                                                                               -->
<!--  Miwako Tsuji                                                                 -->
<!--        miwako.tsuji@riken.jp                                                  -->
<!--        RIKEN R-CCS Center for Computational Science                           -->
<!--        7-1-26 Minatojima-minami-machi, Chuo-ku, Kobe, Hyogo 650-0047, Japan   -->
<!--                                                                               -->
<!--  This software is a computer program whose purpose is to study data           -->
<!--  management for Multi SPMD programming                                        -->
<!--                                                                               -->
<!--  This software is governed by the CeCILL license under French law and         -->
<!--  abiding by the rules of distribution of free software.  You can  use,        -->
<!--  modify and/ or redistribute the software under the terms of the  CeCILL      -->
<!--  license as circulated by CEA, CNRS and INRIA at the following URL            -->
<!--  "http://www.cecill.info".                                                    -->
<!--                                                                               -->
<!--  As a counterpart to the access to the source code and  rights to copy,       -->
<!--  modify and redistribute granted by the license, users are provided only      -->
<!--  with a limited warranty  and the software's author,  the holder of the       -->
<!--  economic rights,  and the successive licensors  have only  limited           -->
<!--  liability.                                                                   -->
<!--                                                                               -->
<!--  In this respect, the user's attention is drawn to the risks associated       -->
<!--  with loading,  using,  modifying and/or developing or reproducing the        -->
<!--  software by the user in light of its specific status of free software,       -->
<!--  that may mean  that it is complicated to manipulate,  and  that  also        -->
<!--  therefore means  that it is reserved for developers  and  experienced        -->
<!--  professionals having in-depth computer knowledge. Users are therefore        -->
<!--  encouraged to load and test the software's suitability as regards their      -->
<!--  requirements in conditions enabling the security of their systems and/or     -->
<!--  data to be ensured and,  more generally, to use and operate it in the        -->
<!--  same conditions as regards security.                                         -->
<!--                                                                               -->
<!--  The fact that you are presently reading this means that you have had         -->
<!--  knowledge of the CeCILL license and that you accept its terms.               -->
<!--                                                                               -->
<component
   type="impl"
   abstract="pdr"
   name="pdr">
  <impl lang="XMP" nodes="CPU:(NBPROCTOT)">
    <templates>
    </templates>
    <header>
      <![CDATA[
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<mpi.h>
#include <xmp.h>

      ]]>
    </header>
    <source>
      <![CDATA[

  int      myrank, nprocs;
  int      myid;
  char     msg[256];
  char     port[MPI_MAX_PORT_NAME];
  char     service_name[MPI_MAX_PORT_NAME];

  // declaration for mapping 
  char     originMapping[12];
  char     newMapping[12];
  char     ieType[12];
  int      i,j,n;
  char *data_loc_remap; // data copy in other distribution handle by server
  int nx, ny;           // local size of original mapping
  int nx_new, ny_new;   // local size for new mapping
  int nx_old, ny_old;   // local size of old mapping
  int px, py;           // number of nodes in direction x and y
  int px_new, py_new;   // number of nodes in direction y and y for new mapping
  int px_old, py_old;   // number of nodes in direction y and y of old mapping

  MPI_Comm row_subcomm; // cub communicator -> row in a cartesian grid
  int row_rank, row_size;
  int sr; //iterator on row_rank

  //end declaration for mapping 

  MPI_Comm   myclient;
  MPI_Status stat;
  MPI_Info   info;

  char *data_loc; // data handle by server
  int count; // size of data_loc
 
  int request=0; // -1 := stop listening, 0 := listen for client import, 1 := listen for client export, 2 := listen for client remapping
  int request_status;

  //  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  myid = myrank;
  
  //printf("server-%04d: myrank=%4d nprocs=%4d\n",myid,myrank,nprocs);

  memset(service_name, 0, sizeof(char)*MPI_MAX_PORT_NAME);
  sprintf(service_name, "serv%d%d%d",dataId, coord, myid);

  // In case of global scope requred for MPI_Lookup_name 
  //MPI_Info_create(&info); 
  //MPI_Info_set(info, "ompi_global_scope", "true");
  MPI_Open_port(MPI_INFO_NULL, port);
  MPI_Publish_name(service_name, MPI_INFO_NULL, port);
  printf("server-%04d_%d: opend port: %s %s\n",myid, myrank, service_name, port);
 
  // Allocation considering info sent by the worker at initialization
  MPI_Comm_accept(port, MPI_INFO_NULL, 0, MPI_COMM_SELF, &myclient);
  MPI_Recv(&count, 1, MPI_INT, 0, 0, myclient, &stat);
  data_loc = (char *)malloc(sizeof(char)*count);
  MPI_Comm_disconnect(&myclient);

  // NEEDS IF REMAPPING BB -> RB 
  // Original matrix is SQUARE
  // Original mapping (Should be given by a component, or internally)
  sprintf(originMapping,"block-block");

  // setup of original block-block grid
  px = sqrt(nprocs); // should be given by a component
  py = px; // in case of square
  nx = sizeof(double)*sqrt(count/sizeof(double)); // Global Matrix is square
  ny = sizeof(double)*sqrt(count/sizeof(double)); // Global Matrix is square
  
  n  = nx*px;

  // creation of subcommunicator needed for remapping
  int color = myrank/py;
  MPI_Comm_split(MPI_COMM_WORLD, color, myrank, &row_subcomm);
  MPI_Comm_rank(row_subcomm, &row_rank);
  MPI_Comm_size(row_subcomm, &row_size);

  // Allocate memory for data copy during remapping
  // Assume it has the same local size (true for even number and square matrix) 
  data_loc_remap = (char *)malloc(sizeof(char)*count);



  int iter=0;

  while (request != -1) {
    iter++;
 
    MPI_Comm_accept(port, MPI_INFO_NULL, 0, MPI_COMM_SELF, &myclient);

    // Receive listening request
    MPI_Recv(&request, 1, MPI_INT, 0, 0, myclient, &stat);
    
    // Receive a bloc matrix (client calls for export)
    if (request == 1) {
      
      MPI_Recv(data_loc, count, MPI_CHAR, 0, 0, myclient, &stat);
            
      // Notify
      request_status = 1;
      MPI_Send(&request_status, 1, MPI_INT, 0, 0, myclient);
    }

    // Send a bloc matrix (client calls for import)
    if (request == 0) {
      MPI_Send(data_loc, count, MPI_CHAR, 0, 0, myclient);
    }

    if (request == 2) {

      // receive kind of mapping transform (originMapping and newMapping)
      // "block-block" "row-block" "column-block"
      MPI_Recv(originMapping, 12, MPI_CHAR, 0, 0, myclient, &stat);
      MPI_Recv(newMapping, 12, MPI_CHAR, 0, 0, myclient, &stat);

      // remap BB -> RB (block-block -> row-block)
      if ( !(strcmp(originMapping,"block-block")) && !(strcmp(newMapping,"row-block")) ){
        fprintf(stderr,"PDR performs remapping  %s  TO  %s \n",originMapping,newMapping);
        
        // compute local size for new mapping
        px_new = nprocs;
        py_new = 1;
        nx_new = n/px_new;
        ny_new = n;

        for (sr=0; sr<row_size; sr++){
          for (i=0; i<nx_new/sizeof(double); i++){
            MPI_Gather(&data_loc[((sr*nx_new/sizeof(double))+i)*ny], ny, MPI_CHAR, &data_loc_remap[i*ny_new], ny, MPI_CHAR, sr, row_subcomm);
          }
        }
        for (j=0; j<count; j++){
            data_loc[j]=data_loc_remap[j];
        }
      }

      // remap RB -> BB (row-block -> block-block)
      if ( !(strcmp(originMapping,"row-block")) && !(strcmp(newMapping,"block-block")) ){
        fprintf(stderr,"PDR performs remapping  %s  TO  %s \n",originMapping,newMapping);
        
        // compute local size for old mapping
        px_old = nprocs;
        py_old = 1;
        nx_old = n/px_old;
        ny_old = n;

        for (sr=0; sr<row_size; sr++){
          for (i=0; i<nx_old/sizeof(double); i++){
            MPI_Scatter(&data_loc[i*ny_old], ny, MPI_CHAR, &data_loc_remap[((sr*nx_old/sizeof(double))+i)*ny], ny, MPI_CHAR, sr, row_subcomm);
          }
        }
        for (j=0; j<count; j++){
            data_loc[j]=data_loc_remap[j];
        }
      }
    
      MPI_Barrier(MPI_COMM_WORLD);

      // Notify
      request_status = 1;
      MPI_Send(&request_status, 1, MPI_INT, 0, 0, myclient);

    }

    if (request == 3) {

      // Remap and Import or export
      // if import do not touch data_loc, keep the mapping. remap data_loc in data_loc_remap Only send data_loc_remap
      // if export, receive in data_loc and then remap dataloc in data_loc_remap. Finally, copy data_loc_remap in data_loc
 
      // receive kind of mapping transform (originMapping and newMapping)
      // "block-block" "row-block" "column-block"
      MPI_Recv(originMapping, 12, MPI_CHAR, 0, 0, myclient, &stat);
      MPI_Recv(newMapping, 12, MPI_CHAR, 0, 0, myclient, &stat);

      // receive kind of exchange "import" or "export"
      MPI_Recv(ieType, 12, MPI_CHAR, 0, 0, myclient, &stat);
 
      if ( !(strcmp(ieType,"export")) ){
        // receive data_loc in its originMapping
        MPI_Recv(data_loc, count, MPI_CHAR, 0, 0, myclient, &stat);
        fprintf(stderr,"PDR received EXPORT before remapping  %s  TO  %s \n",originMapping,newMapping);
  
        MPI_Barrier(MPI_COMM_WORLD);
      }

      // remap BB -> RB (block-block -> row-block)
      if ( !(strcmp(originMapping,"block-block")) && !(strcmp(newMapping,"row-block")) ){
        fprintf(stderr,"PDR performs remapping  %s  TO  %s \n",originMapping,newMapping);
        
        // compute local size for new mapping
        px_new = nprocs;
        py_new = 1;
        nx_new = n/px_new;
        ny_new = n;

        for (sr=0; sr<row_size; sr++){
          for (i=0; i<nx_new/sizeof(double); i++){
            MPI_Gather(&data_loc[((sr*nx_new/sizeof(double))+i)*ny], ny, MPI_CHAR, &data_loc_remap[i*ny_new], ny, MPI_CHAR, sr, row_subcomm);
          }
        }

        if ( !(strcmp(ieType,"export")) ){
          for (j=0; j<count; j++){
            data_loc[j]=data_loc_remap[j];
          }
        }

      }

      // remap RB -> BB (row-block -> block-block)
      if ( !(strcmp(originMapping,"row-block")) && !(strcmp(newMapping,"block-block")) ){
        fprintf(stderr,"PDR performs remapping  %s  TO  %s \n",originMapping,newMapping);

        // compute local size for old mapping
        px_old = nprocs;
        py_old = 1;
        nx_old = n/px_old;
        ny_old = n;

        for (sr=0; sr<row_size; sr++){
          for (i=0; i<nx_old/sizeof(double); i++){
            MPI_Scatter(&data_loc[i*ny_old], ny, MPI_CHAR, &data_loc_remap[((sr*nx_old/sizeof(double))+i)*ny], ny, MPI_CHAR, sr, row_subcomm);
          }
        }

        if ( !(strcmp(ieType,"export")) ){
          for (j=0; j<count; j++){
            data_loc[j]=data_loc_remap[j];
          }

        }

      }

      if ( !(strcmp(ieType,"import")) ){
        // send data_loc in its newMapping (_remap)
        MPI_Send(data_loc_remap, count, MPI_CHAR, 0, 0, myclient);
      }
      else{

        MPI_Barrier(MPI_COMM_WORLD);

        // Notify
        request_status = 1;
        MPI_Send(&request_status, 1, MPI_INT, 0, 0, myclient);
      }

    }

    MPI_Comm_disconnect(&myclient);
  }

  if (request == -1) {
    fprintf(stderr,"server-%04d: Stop import/export %s \n", myid, service_name);
  }

  MPI_Unpublish_name(service_name, MPI_INFO_NULL, port);
  
//  MPI_Finalize();

]]>
    </source>
  </impl>
</component>
