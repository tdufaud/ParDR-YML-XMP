#!/bin/sh
cd ../MPI-client-server_from_spawn/
echo "1 compile client_spec"
make all2
cd ../yml_test/
echo "2 generate query files"
sh generate-bgj-mixIO-with-parameters.sh
cd pdr_components/pdr_abst/
echo "3 register pdr abst components"
./compile &> compile_test; grep ERROR compile_test;
cd ../pdr_impl/
echo "4 register pdr impl components"
./compile &> compile_test; grep ERROR compile_test; grep Exception compile_test;
cd ../../bgj_mixIO_pdr/abst/
echo "5 register bgj abst components"
./compile &> compile_test; grep ERROR compile_test;
cd ../impl/
echo "6 register bgj impl components"
./compile &> compile_test; grep ERROR compile_test; grep Exception compile_test;
cd ../../app/
echo "7 compile gaussJordan app"
yml_compiler gaussJordanMixIO.query
echo "8 register stubs"
omrpc-register-yml gaussJordanMixIO.query.yapp | tail -10 > ~/.omrpc_registry/stubs
