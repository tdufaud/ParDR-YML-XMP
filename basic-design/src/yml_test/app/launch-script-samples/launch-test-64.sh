#!/bin/bash
#@ class = clallmds+
#@ job_name = RUN_cpu88_n8000
#@ total_tasks = 144
#@ node = 9
#@ as_limit = 1.75gb
#@ node_usage = not_shared
#
#@ wall_clock_limit = 00:10:00
#@ output = $(job_name).$(jobid).log
#@ error = $(job_name).$(jobid).err
#@ job_type = mpich
#@ environment = COPY_ALL
#@ queue
#
module load gnu/5.4.0 gnu-env/5.4.0 intel intelmpi autotools/Feb2014
export I_MPI_DAPL_PROVIDER=ofa-v2-ib0
export TASKS_PER_NODE=`echo $LOADL_PROCESSOR_LIST|sed -e s/" "/\\n/g|uniq|wc -l`
mpirun -n 1 ./master-xmpclient
