#!/bin/bash
#@ class = clallmds+
#@ job_name = RUN_lessCPU_add_cpu33_nx1000
#@ total_tasks = 64
#@ node = 4
#@ as_limit = 1.75gb
#@ node_usage = not_shared
#
#@ wall_clock_limit = 00:10:00
#@ output = $(job_name).$(jobid).log
#@ error = $(job_name).$(jobid).err
#@ job_type = mpich
#@ environment = COPY_ALL
#@ queue
#
module load gnu/5.4.0 gnu-env/5.4.0 intel intelmpi autotools/Feb2014
export I_MPI_DAPL_PROVIDER=ofa-v2-ib0
echo $LOADL_PROCESSOR_LIST | sed -e s/" "/\\n/g | sed -e s/"poincare"/"192.168.150."/g | sed -e s/"-adm.maisondelasimulation.fr"/""/g 
echo $LOADL_PROCESSOR_LIST | sed -e s/" "/\\n/g | sed -e s/"poincare"/"192.168.150."/g | sed -e s/"-adm.maisondelasimulation.fr"/""/g > ~/.omrpc_registry/nodes
#echo $LOADL_PROCESSOR_LIST | sed -e s/" "/\\n/g > ~/.omrpc_registry/nodes
time mpirun -n 1 yml_scheduler testAdd.query.yapp