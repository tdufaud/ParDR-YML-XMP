#
# - file: redirection of standard output of testA.query
# read 5th column and sum
#
BEGIN {
    FS=" "
    time=0
}
{
    if ( $1=="Import" || $1=="Export" ){ 
	time+=$2
    }
}
END {
    print time 
}
