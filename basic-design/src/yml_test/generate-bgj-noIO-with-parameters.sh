#!/bin/sh
# change generic parameters in .query.GEN files
# and generates .query files
# 
NBPROCI=3
NBPROCTOT=9
NBELTLOCX=900
RBNBELTLOCX=300
NX=$((NBPROCI*NBELTLOCX))

sed "s/PX/${NBPROCI}/g" bgj_noIO_pdr/impl/XMP_copyMat.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > bgj_noIO_pdr/impl/XMP_copyMat.query
sed "s/PX/${NBPROCI}/g" bgj_noIO_pdr/impl/XMP_fillMatrixZero.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > bgj_noIO_pdr/impl/XMP_fillMatrixZero.query
sed "s/PX/${NBPROCI}/g" bgj_noIO_pdr/impl/XMP_genMat.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > bgj_noIO_pdr/impl/XMP_genMat.query
sed "s/NBPROCTOT/${NBPROCTOT}/g" bgj_noIO_pdr/impl/XMP_inversion.query.GEN | sed "s/RBNBELTLOCX/${RBNBELTLOCX}/g" > bgj_noIO_pdr/impl/XMP_inversion.query 
sed "s/PX/${NBPROCI}/g" bgj_noIO_pdr/impl/XMP_mProdMat.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > bgj_noIO_pdr/impl/XMP_mProdMat.query
sed "s/PX/${NBPROCI}/g" bgj_noIO_pdr/impl/XMP_prodDiff.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > bgj_noIO_pdr/impl/XMP_prodDiff.query
sed "s/PX/${NBPROCI}/g" bgj_noIO_pdr/impl/XMP_prodMat.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > bgj_noIO_pdr/impl/XMP_prodMat.query

sed "s/NBPROCTOT/${NBPROCTOT}/g" pdr_components/pdr_impl/allocateInPdr_impl.query.GEN > pdr_components/pdr_impl/allocateInPdr_impl.query
sed "s/NBPROCTOT/${NBPROCTOT}/g" pdr_components/pdr_impl/pdr_impl.query.GEN | sed "s/PX/${NBPROCI}/g" > pdr_components/pdr_impl/pdr_impl.query
sed "s/NBPROCTOT/${NBPROCTOT}/g" pdr_components/pdr_impl/stopPdr_impl.query.GEN > pdr_components/pdr_impl/stopPdr_impl.query
sed "s/PX/${NBPROCI}/g" pdr_components/pdr_impl/move2IOdr.query.GEN | sed "s/NX/${NX}/g" | sed "s/NBELTLOCX/${NBELTLOCX}/g" > pdr_components/pdr_impl/move2IOdr.query

sed "s/NBELTLOCX/${NBELTLOCX}/g" app/gaussJordanNoIO.query.GEN > app/gaussJordanNoIO.query
