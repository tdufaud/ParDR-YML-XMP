#!/bin/sh
# change generic parameters in initA_impl.query.GEN  initPdr_impl.query.GEN  pdr_impl.query.GEN  read_impl.query.GEN  stopPdr_impl.query.GEN
# and generates:
# initA_impl.query  initPdr_impl.query  pdr_impl.query  read_impl.query  stopPdr_impl.query
# 
NBPROCI=2
NBPROCTOT=4
NBELTLOCX=2
RBNBELTLOCX=1

sed "s/PX/${NBPROCI}/g" simple/impl/initA_impl.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > simple/impl/initA_impl.query
sed "s/PX/${NBPROCI}/g" simple/impl/read_impl.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > simple/impl/read_impl.query
sed "s/PX/${NBPROCI}/g" simple/impl/initForAdd_impl.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > simple/impl/initForAdd_impl.query
sed "s/PX/${NBPROCI}/g" simple/impl/add_impl.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > simple/impl/add_impl.query 
sed "s/NBPROCTOT/${NBPROCTOT}/g" simple/impl/readRemap_impl.query.GEN | sed "s/RBNBELTLOCX/${RBNBELTLOCX}/g" > simple/impl/readRemap_impl.query
sed "s/NBPROCTOT/${NBPROCTOT}/g" simple/impl/readRemapIE_impl.query.GEN | sed "s/RBNBELTLOCX/${RBNBELTLOCX}/g" > simple/impl/readRemapIE_impl.query
sed "s/PX/${NBPROCI}/g" simple/impl/finalizeA_impl.query.GEN | sed "s/NBELTLOCX/${NBELTLOCX}/g" > simple/impl/finalizeA_impl.query

sed "s/NBPROCTOT/${NBPROCTOT}/g" pdr_components/pdr_impl/allocateInPdr_impl.query.GEN > pdr_components/pdr_impl/allocateInPdr_impl.query
sed "s/NBPROCTOT/${NBPROCTOT}/g" pdr_components/pdr_impl/pdr_impl.query.GEN | sed "s/PX/${NBPROCI}/g" > pdr_components/pdr_impl/pdr_impl.query
sed "s/NBPROCTOT/${NBPROCTOT}/g" pdr_components/pdr_impl/stopPdr_impl.query.GEN > pdr_components/pdr_impl/stopPdr_impl.query

sed "s/NBELTLOCX/${NBELTLOCX}/g" app/testA.query.GEN > app/testA.query
sed "s/NBELTLOCX/${NBELTLOCX}/g" app/testAdd.query.GEN > app/testAdd.query
sed "s/NBELTLOCX/${NBELTLOCX}/g" app/testARemap.query.GEN > app/testARemap.query
sed "s/NBELTLOCX/${NBELTLOCX}/g" app/testARemapIE.query.GEN > app/testARemapIE.query